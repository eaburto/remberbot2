from datetime import datetime
import constants
from models import User, Reminder, db
import models
import dateparser
import logging
import pytz
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

LOGGER = logging.getLogger(__name__)


def remindmecmd_callback(update: Update, context: CallbackContext):
    if not update.message:
        return
    msg = update.message
    if not msg.text or not msg.from_user:
        return

    with db:
        user, _ = User.get_or_create(id=msg.from_user.id)
        usertz = pytz.timezone(user.timezone)

        clean_msg = msg.text.split(' ', 1)[1]

        remindertime = dateparser.parse(clean_msg, settings={'PREFER_DATES_FROM': 'current_period',
                                                             'DATE_ORDER': 'DMY',
                                                             'PREFER_DAY_OF_MONTH': 'first',
                                                             'TIMEZONE': user.timezone,
                                                             'TO_TIMEZONE': 'UTC'})
        if not remindertime:
            update.message.reply_text("No entendí, lo siento :(. Inténtalo de nuevo con otras palabras!", quote=True)
            return
        if remindertime <= datetime.utcnow():
            update.message.reply_text("Esta fecha está en el pasado, no se pudo programar el recordatorio", quote=True)
            return

        # Valid request
        Reminder.create(msgid=msg.message_id,
                        msgtxt=msg.text,
                        chatid=msg.chat.id,
                        userid=msg.from_user.id,
                        remindertime=remindertime)
        localremindertime = pytz.utc.localize(remindertime).astimezone(usertz)
        fmt = "%d-%m-%Y %H:%M:%S UTC%z"
        update.message.reply_text("Recordatorio creado exitosamente!\n" + localremindertime.strftime(fmt), quote=True)


def notify_callback(context: CallbackContext):
    with db:
        reminders = Reminder.select().where(Reminder.remindertime < datetime.utcnow())
        for r in reminders:
            msg = context.bot.send_message(chat_id=r.chatid,
                                           reply_to_message_id=r.msgid,
                                           text="Se cumplió la hora de este recordatorio programado!")
            if msg:
                Reminder.delete_by_id(r.id)


def settz_callback(update: Update, context: CallbackContext):
    if not update.message:
        return
    msg = update.message
    if not msg.text or not msg.from_user:
        return

    tzname = msg.text[6:].strip()

    if tzname not in pytz.all_timezones:
        update.message.reply_text("Esta no es una timezone válida", quote=True)
        return

    with db:
        user, _ = User.get_or_create(id=msg.from_user.id)
        user.timezone = tzname
        user.save()
        update.message.reply_text("Timezone cambiada exitosamente a '" + tzname + "'", quote=True)


def cancelreminder_callback(update: Update, context: CallbackContext):
    if not update.message:
        return
    msg = update.message
    if not msg.from_user and not msg.reply_to_message:
        return

    userid = msg.from_user.id
    msgid = msg.reply_to_message.message_id
    chatid = msg.reply_to_message.chat_id

    with db:
        q = Reminder.delete().where((Reminder.userid == userid) &
                                    (Reminder.msgid == msgid) &
                                    (Reminder.chatid == chatid))
        qty = q.execute()
    update.message.reply_text("Se canceló " + str(qty) + " recordatorio(s).", quote=True)


if __name__ == '__main__':
    models.create_tables()
    updater = Updater(token=constants.API_KEY, use_context=True)
    jq = updater.job_queue
    jq.run_repeating(notify_callback, interval=30, first=5)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("remindme", remindmecmd_callback))
    dp.add_handler(CommandHandler("settz", settz_callback))
    dp.add_handler(CommandHandler("cancelreminder", cancelreminder_callback))
    aps_logger = logging.getLogger('apscheduler')
    aps_logger.setLevel(logging.WARNING)
    updater.start_polling()
    updater.idle()
