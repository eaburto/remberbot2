import datetime
import logging

from peewee import *

import constants
from constants import DB_FILE

db = SqliteDatabase(DB_FILE)
LOGGER = logging.getLogger(__name__)


def create_tables():
    with db:
        db.create_tables([User, Reminder])


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    id = IntegerField(primary_key=True)
    timezone = CharField(default=constants.DEFAULT_TZ)

    def exists(id: int) -> bool:
        result = User.get_or_none(User.id == id)
        if result:
            return True
        return False


class Reminder(BaseModel):
    id = AutoField()
    msgid = IntegerField()
    msgtxt = CharField()
    chatid = IntegerField()
    userid = ForeignKeyField(User, backref='reminders', on_delete='CASCADE')
    creationtime = DateTimeField(default=datetime.datetime.utcnow)
    remindertime = DateTimeField()
